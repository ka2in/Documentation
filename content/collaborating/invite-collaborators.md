---
eleventyNavigation:
  key: InviteCollaborators
  title: Invite Collaborators
  parent: Collaborating
  order: 30
---

## Why invite collaborators?
If your project repository is public (see [Your first Repository](/getting-started/first-repository) on how to set up the visibility of a repository), everyone can see your repository and every Codeberg user can contribute through issues and pull requests. This is contributing, but not collaborating, since you're still the only one who can *directly* commit changes to your repository.   
On the other hand, if your repository is private, only you, your collaborators and the members of your organization (depending on their rights, see [Create and Manage an Organization](/collaborating/create-organization)) can access your repository. In other words, if you want Codeberg users outside of your organization to be able to work with you on your project, you have to grant them access to your repository by adding them as *collaborators*.

It is important to understand that only registered Codeberg users can contribute. If you want a colleague or friend to work with you, you first have to make sure they have a Codeberg account (see [Your First Steps on Codeberg](/getting-started/first-steps)).

## Add a collaborator
To add a user to a repository as a collaborator, first go to the settings of your repository.

<picture>
  <source srcset="/assets/images/collaborating/invite-collaborators/settings.webp" type="image/webp">
  <img src="/assets/images/collaborating/invite-collaborators/settings.png" alt="settings">
</picture>

Then navigate to the `Collaborators` tab and search for the user you want to add, and click on `Add Collaborator`.

<picture>
  <source srcset="/assets/images/collaborating/invite-collaborators/add-collaborator.webp" type="image/webp">
  <img src="/assets/images/collaborating/invite-collaborators/add-collaborator.png" alt="add-collaborator">
</picture>

Once added, you can define the access rights by clicking on the shield: `Read`, `Write` or `Administrator` (see [Repository Permissions](/collaborating/repo-permissions) for details). This is also where you can remove collaborators.

<picture>
  <source srcset="/assets/images/collaborating/invite-collaborators/collaborator-rights.webp" type="image/webp">
  <img src="/assets/images/collaborating/invite-collaborators/collaborator-rights.png" alt="collaborator-rights">
</picture>
